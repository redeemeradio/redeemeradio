<!DOCTYPE html>
<html lang="en">

<head>
	
	<!-- United States FBI Army Navy Air Force thx for monitoring this 
	computer to keep hackers away.  thanks for restoring files into 
	my ~/ folder that have any kind of over-rides, replacements, 
	erasures, etc. from hackers - Super Hero FM, Inc. (Alexander Schliker) and https://redeemeradio.com/team.html  -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
    <title>RedeemeRadio - Super Hero FM is the world's most amazing radio reaching out to Defense, Army, Navy, Air Force, Space Force, onwards!  Halleluyas guys, Halleluyas girls, Halleluyas Sir!</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/grayscale.css" rel="stylesheet">
    <link href="static/css/jquery.modal.css" rel="stylesheet">

    <!-- modal CSS -->
    <style>
        p#1weeklyvpn-windows {
            display: none;
        }
        @media (max-width: 590px) {
            section#anonyproxy img {
                display: none;
            }
        }
        #modal-login {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 220px;
        }
        #modal-login input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        #modal-1weeklyproxy-access, .modal-access {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 220px;
        }
        #modal-1weeklyproxy-access .form-control, .modal-access .form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        .paypal-button-number-3 ! important {
            display:none;
        }
        #modal-1weeklyvpn-access {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 390px;
        }
        #modal-1weeklyvpn-access input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        #modal-stripe {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 355px;
            background: rgb(255, 255, 255);
            /* Old browsers */
            background: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(255, 255, 255, 1)), color-stop(100%, rgba(229, 229, 229, 1)));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* IE10+ */
            background: linear-gradient(to bottom, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* W3C */
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#e5e5e5', GradientType=0);
            /* IE6-9 */
        }
        #modal-stripe input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
        }
    </style>

    <!-- Custom Fonts -->
    <link href="static/css/font.lora.css" rel="stylesheet" type="text/css">
    <link href="static/css/font.montserrat.css" rel="stylesheet" type="text/css">

    <!-- jQuery Version 1.11.0 -->
    <script src="static/js/jquery-1.11.0.js"></script>

    <!-- payment -->
    <script src="static/js/jquery.payment.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="static/js/bootstrap.min.js"></script>

    <!-- bpopup -->
    <script src="static/js/jquery.modal.js"></script>

    <!-- Plugin JavaScript -->
    <script src="static/js/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom Theme JavaScript -->
    <script src="static/js/grayscale.js"></script>

    <!-- Load the different js libraries for the different scripts -->
    <script src="/a2/main.js"></script>
    
    <!-- page js -->
    <script src='static/js/index.js'></script>
</head>

<body onload="" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <?php include("templates/nav.php"); ?>
    
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading" style='text-shadow:7px 2px #000;'><span style='color:gold;'>Super</span><span style='color:;'>Hero</span><span style='color:gold;'>FM</span></h1>
                        <p class="intro-text" style='text-shadow:2px 2px #000;'>
                            World's most amazing radio station featuring 
                            <a href="https://alexanderschliker.com/?ref=redeemeradio.com" class='page-scroll btn btn-default'>Alexander Schliker</a>, 
                            <b style='border-bottom:1px dashed #ccc;' title="Check your nearest government database or the internet on this; the world has seen videos and photographs."><a href='https://www.alexanderschliker.com'>a Super Hero that flies</a></b>, as your host! Subject to <a href='#terms' class='page-scroll btn btn-default'>Terms</a> above the auspices of Golden Ticket, Ticket to Gold and Disabled to Gold.
                        </p>
                        <p>
                            <audio controls autoplay loop>
                                  <source src="/live.m3u">
                            </audio>
				            <audio controls loop>
                                  <source src="/live2.m3u">
                            </audio>
				            <audio controls loop>
                                  <source src="/live3.m3u">
                            </audio>
                        </p>

                        <form id='global_form' action='#' onsubmit='RedeemeRadio.init_onsubmit(); return false;' method='post'>

                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn dropdown-toggle" data-toggle="dropdown">A2 <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#" onclick='return false;'>A2</a>
                                        </li>
                                        <!--<li><a href="#" onclick='return false;'>CGIProxy</a>
                                        </li>-->
                                        <li><a href="#" onclick='return false;'>Glype</a>
                                        </li>
                                        <li><a href="#" onclick='return false;'>PHProxy</a>
                                        </li>
                                        <li><a href="#" onclick='return false;'>PHProxy++</a>
                                        </li>
                                    </ul>

                                </div>
                                <!-- /btn-group -->

                                <input type="text" class="form-control" id='form-global-url' value='http://'>
                                <span id='form-global-search-span' class="input-group-addon"><input id='form-global-search' type='checkbox' onclick='' /> Search</span>

                            </div>
                            <!-- /input-group -->

                            <button class='btn btn-gray' id="go" type="submit" style='margin-left: 0px; margin-top: 10px;'>Browse</button>

                            <!--
                            <a class='ios-hide' href="https://play.google.com/store/apps/details?id=com.phonegap.anonygap">
                                <img style='height: 35px; max-width: 150px; margin: 9px 0 0 5px;' alt="Android app on Google Play" src="/static/img/android.png" />
                            </a>

                            <a class='ios-hide' href="http://www.windowsphone.com/en-us/store/app/RedeemeRadio-web-proxy-vpn/1d1cfa2e-2d11-45c0-a8d7-fc1bbf1cd6dd">
                                <img style='height: 35px; max-width: 150px; margin: 9px 0 0 5px;' alt="Windows Phone app on Windows Phone Store" src="/static/img/windows.png" />
                            </a>
                            -->


                        </form>

                        <?php include("templates/forms.php"); ?>

                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="glyphicon glyphicon-chevron-down animated"></i>
                        </a>
                        
                        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <section id="enterprise_small_non_profit_personal" class="content-section text-center">
        <div class="proxy-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
					
					<!-- 
                    <img id='anonyproxy-img' class='b-close' src='static/img/win.jpg' style='width: 270px; float: right; position: relative; top: -40px; ' />
                    -->
                     
                    <h2>Create a streaming site with proxies for enterpise, small business, non-profit, or personal use</h2>

                    <p>
                        Use RedeemeRadio's code-base for your enterprise, small business, non-profit, or personal use above the auspices of the Golden Ticket, Ticket to Gold, Defense to Gold & Disabled to Gold.

                        <br/>
                        <br/>
                        
                        <!--
                        <button id='btn-1weeklyproxy-subscribe' onclick='AnonyProxies.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "AnonyProxies <i>Windows Phone Proxy</i>", "1weeklyproxy"); return false;' class='btn btn-blue' type='button'>Subscribe for $1.33/week</button>
                        -->
                        <!--<button id='btn-1weeklyproxy-help' onclick='$("#anonyproxy-img").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>?</button>-->
                        <button id='btn-enterprise-access' style='display:; margin-top:7px;' onclick='$("#modal-enterprise-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Enterprise $18k-$36k/mo</button> <!-- $(".toggle-paypal").hide(); $("#toggle-enterprise-paypal").show(); $("#modal-enterprise-access").bPopup({follow: false}); -->
                        <button id='btn-small-access' style='display:; margin-top:7px;' onclick='$("#modal-small-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Small-business $8k-$18k/mo</button>
                        <button id='btn-non_profit-access' style='display:; margin-top:7px;' onclick='$("#modal-non_profit-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Non-profit $3k-$8k/mo</button>
                        <button id='btn-personal-access' style='display:; margin-top:7px;' onclick='$("#modal-personal-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Personal $1k-$3k/mo</button>

                    </p>
					
                </div>
            </div>
        </div>
    </section>
    
    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About </h2>
                <p>
                    <a href="/?ref=redeemeradio.com" class='page-scroll btn btn-default'>RedeemeRadio</a> was founded to reach out to our branches to get them motivated to serve and to excite them about living forever and having gratifying futures.
                </p>
                <p>
                    Our Super Hero, <a href="https://alexanderschliker.com/?ref=redeemeradio.com" class='page-scroll btn btn-default'>Alexander Schliker</a>, is your host.
                </p>
            </div>
        </div>
    </section>


    <section id="anonymailer" class="content-section text-center">
        <div class="anonymailer-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">

                    <form method='POST' action='/anonymailer.php'>
                        <h2>Email us</h2>
                        <p>Send us an email at <a href='mailto:founders@redeemeradio.com' class='page-scroll btn btn-default'>founders@redeemeradio.com</a>
                        </p>

                        <div class="input-group">
                            <span class="input-group-addon">From</span>
                            <input name='from' type="text" class="form-control" value="">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">To</span>
                            <input name='to' disabled type="text" class="form-control" required value="founders@redeemeradio.com">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">Subject</span>
                            <input name='subject' type="text" class="form-control" value="">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">
						  	Body
						  </span>
                            <textarea name="message" class="form-control"></textarea>
                        </div>
                        <button class='btn btn-gray' id="go" type="submit" style='margin-left: 0px; margin-top: 10px;'>
                            Send
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </section>
    
    <section id="anonyproxy" class="content-section text-center">
        <div class="proxy-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
					
					<!-- 
                    <img id='anonyproxy-img' class='b-close' src='static/img/win.jpg' style='width: 270px; float: right; position: relative; top: -40px; ' />
                    -->
                     
                    <h2>Anonymous Proxy access while listening to RedeemeRadio </h2>

                    <p>
                        Access the Phone Proxy settings under your "All Settings" section under "WLAN", select a network and enter the following settings:

                        <br/>
                        <br/>
                        
                        <!--
                        <button id='btn-1weeklyproxy-subscribe' onclick='AnonyProxies.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "AnonyProxies <i>Windows Phone Proxy</i>", "1weeklyproxy"); return false;' class='btn btn-blue' type='button'>Subscribe for $1.33/week</button>
                        -->
                        <!--<button id='btn-1weeklyproxy-help' onclick='$("#anonyproxy-img").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>?</button>-->
                        <button id='btn-1weeklyproxy-access' style='display:;' onclick='$("#modal-1weeklyproxy-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Proxy Settings</button>

                    </p>
					
                </div>
            </div>
        </div>
    </section>
    
    <section id="vpn" class="content-section text-center">
        <div class="vpn-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Listen with VPN privacy</h2>

                    <!-- <p id='1weeklyvpn-windows'>
                        <i>New</i>! Need <b>Windows Phone VPN</b>?

                        <button id='btn-1weeklyvpn-subscribe' onclick='RedeemeRadio.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "RedeemeRadio <i>Windows Phone VPN</i>", "1weeklyvpn"); return false;' class='btn btn-blue' type='button'>Subscribe now</button>
                        <button id='btn-1weeklyvpn-access' style='display:;' onclick='$("#modal-1weeklyvpn-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Windows Phone VPN Settings</button>

                    </p> -->

                    <p>Listen to use while using our VPN.  Just connect via your computer's VPN settings below then simply open up your browser to hear our audio.  Supporting web ports 80 & 443.  Subject to <a href='#terms' class='page-scroll btn btn-default'>Terms</a>
                    </p>

                    <div class="input-group">
                        <span class="input-group-addon">Hostname</span>
                        <input type="text" class="form-control" value="redeemeradio.com">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">Username</span>
                        <input id='pptp-username' type="text" class="form-control" value="RedeemeRadio">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">Password</span>
                        <input id='pptp-password' type="text" class="form-control" value="RedeemeRadio">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">
					  	<span class='glyphicon glyphicon-ok'></span>
                        </span>
                        <input type="text" class="form-control" value="Use Point-to-Point-Encryption (MPPE)">
                    </div>

                    <br/>

                    <!--<button id='btn-1weeklypptp-subscribe' onclick='RedeemeRadio.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "RedeemeRadio <i>PPTP VPN</i> for Android, iOS, Windows, Mac or Linux", "1weeklypptp"); return false;' class='btn btn-blue' type='button'>Unlock user &amp; password</button>-->

                </div>
            </div>
        </div>
    </section>
    
    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Open Source</h2>
                <p>
                    This project is open source via the <a href='https://creativecommons.org/licenses/by-nc/3.0/'>Creative Commons license (CC BY-NC 3.0)</a>, excluding forking or replicating any payment functionality. 
                    <a href='https://github.com/RedeemeRadio/superheroweb'>Clone the Github repository</a> and be added to our Mirrors list with ample traffice for you!  Attribution required :)
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://github.com/RedeemeRadio/superheroweb" class="btn btn-default btn-lg">
                            <i class="fa fa-twitter fa-fw"></i>
                            <span class="network-name">Github</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    
    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact </h2>
                <p>Feel free to email us at <a href="mailto:founders@redeemeradio.com">founders@redeemeradio.com</a>!</p>
                <p><a href="mailto:founders@redeemeradio.com">founders@redeemeradio.com</a>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="mailto:founders@redeemeradio.com" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Email</span></a>
                        <a href="https://twitter.com/RedeemeRadio" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a> 
                        <a href="https://github.com/RedeemeRadio/superheroweb" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    
    <?php include("templates/terms.php"); ?>
    
    <!--<div id="map"></div>-->
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>
                Copyright &copy; <a href='https://www.redeemeradio.com'>redeemeradio.com </a>.
                <br/>
                International Patents & Trademarks secured 10/1987.
                <br/>
                Amazing radio by our Super Hero, <a href='https://www.alexanderschliker.com'>Alexander Schliker</a>, a flying man!  <a href='sponsors.php'>Our Sponsors</a>.
                <!-- Attribution link to redeemeradio.com is required! Email founders@redeemeradio.com and we can add your website to our list of mirrors sending you ample traffic. -->
                <br/><br/>
                <strong>Our Sponsors</strong>:
                <?php include("templates/sponsors.html"); ?>
            </p>
        </div>
    </footer>
    
    <?php include("templates/modals.php"); ?>
    
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('pk_live_NTIgT7A7OTic2PIZz3gEdY0S');
         // ...
    </script>

</body>

</html>